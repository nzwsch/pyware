import pygame
import glob


class Assets():
    IMAGES = glob.glob("pyware/assets/images/*")

    def __init__(self):
        self.images = [pygame.image.load(a) for a in self.IMAGES]

    def find_images_by(self, file_name):
        for asset, path_name in enumerate(self.images):
            if path_name == file_name:
                return asset
            else:
                raise FileNotFoundError
