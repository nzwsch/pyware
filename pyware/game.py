import pygame
import pyware.title


class Game():
    scene = None
    done = False

    def __init__(self):
        self.scene = pyware.Title()

    def update(self, dt):
        self.done = self.scene.is_done()
        if not self.done: self.scene.update(dt)

    def draw(self, screen):
        screen.fill((0, 0, 0))

        if not self.done: self.scene.draw(screen)

        pygame.display.flip()

    def is_done(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return True
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                return True
