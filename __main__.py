#!/usr/bin/env python

import pygame
import pyware

def main():
    pygame.init()

    # display mode
    screen = pygame.display.set_mode((800, 600))

    # var
    done = False
    clock = pygame.time.Clock()

    # init
    game = pyware.Game()

    while not done:
        done = game.is_done()
        ms = clock.tick(60) / 1000

        # update
        game.update(ms)

        # draw section
        game.draw(screen)

    # quit game
    pygame.quit()


if __name__ == '__main__':
    main()
